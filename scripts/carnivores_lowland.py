import textwrap
from biosim.simulation import BioSim

geogr = """WWW
            WLW
            WWW"""

geogr = textwrap.dedent(geogr)

ini_carns = [{'loc': (2, 2),
              'pop': [{'species': 'Carnivore',
                       'age': 5,
                       'weight': 20}
                      for _ in range(20)]}]

n_years = 301

for seed in range(100, 103):
    sim = BioSim(geogr, ini_carns, seed=seed,
                 img_dir='results', img_base=f'mono_ho_{seed:05d}', img_years=300)
    sim.simulate(n_years)
