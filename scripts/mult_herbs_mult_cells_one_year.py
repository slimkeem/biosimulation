import textwrap
from biosim.simulation import BioSim

geogr = """WWWWWW
           WLDHLW
           WDHLWW
           WHLHDW
           WLWDHW
           WWWWWW"""

geogr = textwrap.dedent(geogr)

ini_herb = [{'loc': (2, 2),
             'pop': [{'species': 'Herbivore',
                      'age': 5,
                      'weight': 20}
                     for _ in range(100)]}]

n_years = 1
ini_pop = ini_herb
for seed in range(100, 103):
    sim = BioSim(geogr, ini_pop, seed=seed,
                 img_dir='results', img_base=f'mono_ho_{seed:05d}', img_years=300)
    sim.simulate(n_years)
