import textwrap
from biosim.simulation import BioSim

geogr = """WWWWWW
           WLDHLW
           WDHLWW
           WHLHDW
           WLWDHW
           WWWWWW"""

geogr = textwrap.dedent(geogr)

ini_carns1 = [{'loc': (2, 2),
              'pop': [{'species': 'Carnivore',
                       'age': 5,
                       'weight': 20}
                      for _ in range(20)]}]

ini_carns2 = [{'loc': (3, 3),
              'pop': [{'species': 'Carnivore',
                       'age': 5,
                       'weight': 20}
                      for _ in range(20)]}]
ini_carns3 = [{'loc': (5, 4),
              'pop': [{'species': 'Carnivore',
                       'age': 5,
                       'weight': 20}
                      for _ in range(20)]}]


n_years = 300
ini_pop = ini_carns3+ini_carns2+ini_carns1
for seed in range(100, 103):
    sim = BioSim(geogr, ini_pop, seed=seed,
                 img_dir='results', img_base=f'mono_ho_{seed:05d}', img_years=300)
    sim.simulate(n_years)
