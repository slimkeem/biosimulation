import textwrap
from biosim.simulation import BioSim

geogr = """WWW
            WLW
            WWW"""
geogr = textwrap.dedent(geogr)

ini_herbs1 = [{'loc': (2, 2),
              'pop': [{'species': 'Herbivore',
                       'age': 5,
                       'weight': 40}
                      for _ in range(20)]}]

ini_carns1 = [{'loc': (2, 2),
               'pop': [{'species': 'Carnivore',
                        'age': 5,
                        'weight': 40}
                       for _ in range(20)]}]

n_years = 300
ini_pop = ini_herbs1+ini_carns1
for seed in range(100, 103):
    sim = BioSim(geogr, ini_pop, seed=seed,
                 img_dir='results', img_base=f'mono_ho_{seed:05d}', img_years=300)
    sim.simulate(n_years)
