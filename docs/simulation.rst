Simulation
==========

The BioSim module
-----------------

This module simulates the population dynamics on an Island.

.. automodule:: biosim.simulation
   :members:

