Island
======

The Island module
-----------------

The Island module provides code for creating an island grid and mapping biomes to their keys and
the annual cycle function that occurs each year on the island.


.. automodule:: biosim.island
   :members:

