Biome
=====

The Biome module
-----------------

The biome module provide code for creating a biome with a landscape type and placing animals there.
This module also implements the different cycle function which occur each year such as
aging, procreation, migration etc.

.. automodule:: biosim.biome
   :members:

