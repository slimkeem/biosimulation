Landscape
=========

The Landscape module
--------------------

The landscape module provides code for four types of landscapes water, desert, lowland and highland.
Lowland contains the highest amounts of fodder, highland some amounts while water and desert do not
contain any fodder. All landscape types beside water are habitable.

.. automodule:: biosim.landscape
   :members:

