.. BioSim documentation master file, created by
   sphinx-quickstart on Sun Jun  6 15:50:46 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BioSim's documentation!
==================================

The BioSim is a simulation of the population dynamics on an island.
This is a computer program for the simulation of
   * an animal
   * a biome of a landscape type and its population
   * an island with several different landscapes types, yearly cycles and its animal population


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   animal
   biome
   helper
   landscape
   island
   simulation

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
