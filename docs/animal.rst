Animal
======

The animal module
-----------------

The animal module provides code for herbivore and carnivore animal.
Both animals have different sets of default parameters and differ in the way they eat.
Herbivores eat fodder while the carnivores eat herbivores.
The animals share different characteristics such as weight calculation, fitness calculation,
migration, birth and death. These characteristics are defined as different equations.

.. automodule:: biosim.animal
   :members:

