import unittest
import textwrap
import biosim.helper as helper
from biosim.landscape import Lowland
from biosim.biome import Herd
from biosim.island import Island


def landscape_grid():
    geogr = """WWW
               WLW
               WWW"""
    geogr = textwrap.dedent(geogr)
    return geogr


def herds_dict_string():
    num_herbs = 5
    ini_herbs = [{'loc': (2, 2),
                  'pop': [{'species': 'Herbivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(num_herbs)]}]
    return ini_herbs


class HelperTestCases(unittest.TestCase):
    def test_extract_herd_dict(self):
        num_herbs = 5
        cell_key = (2, 2)
        ini_herbs = herds_dict_string()
        herd_dict = helper.extract_herd_dict(ini_herbs)
        list_of_herds = herd_dict.get(cell_key)

        for herd in list_of_herds:
            self.assertTrue(len(herd.herd_list) == num_herbs)

    def test_extract_landscape_dict(self):
        cell_key = (2, 2)
        land_string = landscape_grid()
        landscape_dict = helper.extract_landscape_dict(land_string)
        lowland_landscape = landscape_dict.get(cell_key)

        self.assertTrue(type(lowland_landscape) == Lowland)

    def test_get_neighbor_coords(self):
        cell_key = (3, 2)
        new_island = Island(grid=landscape_grid(), herds_string=herds_dict_string())
        all_keys = new_island.get_keys()
        real_neighbor_coords = [(2, 2), (3, 1), (3, 3)]
        check_neighbor_coords = helper.get_neighbor_coords(cell_key, all_keys)

        self.assertTrue(set(real_neighbor_coords) == set(check_neighbor_coords))

    def test_create_new_herd_from_dict(self):
        herd_string = herds_dict_string()
        for new_dict in herd_string:
            new_herd = helper._create_new_herd_from_dict(new_dict)
            self.assertTrue(type(new_herd) == Herd)


if __name__ == '__main__':
    unittest.main()
