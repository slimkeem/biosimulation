import unittest
import textwrap
# import pytest

from biosim.island import Island
from biosim.landscape import Water


def landscape_grid():
    geogr = """WWW
               WLW
               WWW"""
    geogr = textwrap.dedent(geogr)
    return geogr


def herds_dict_string():
    ini_herbs = [{'loc': (2, 2),
                  'pop': [{'species': 'Herbivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(5)]}]
    return ini_herbs


def gen_island():
    geogr = landscape_grid()
    ini_herbs = herds_dict_string()
    simple_island = Island(geogr, ini_herbs)
    return simple_island


def test_run_cycle(mocker):
    """
    Test to check if the annual cycle on the island run with out death and birth,
    checking if age has increased as it should.
    """
    new_island = gen_island()
    living_cell_key = (2, 2)
    animals_before_age = 0
    animals_after_age = 0

    cell_biome = new_island._get_biome(living_cell_key)
    for herd in cell_biome.get_all_herds():
        for animal in herd.herd_list:
            animals_before_age += animal.get_age()/len(herd.herd_list)
    mocker.patch('biosim.animal.Animal.check_death', return_value=False)
    mocker.patch('biosim.animal.Animal.give_birth', return_value=None)
    new_island.run_cycle()
    for herd in cell_biome.get_all_herds():
        for animal in herd.herd_list:
            animals_after_age += animal.get_age()/len(herd.herd_list)

    assert animals_after_age == animals_before_age + 1


class IslandTestCase(unittest.TestCase):
    # constructor tests
    def test_new_island(self):
        """
        Test for new island instance
        """
        new_island = gen_island()
        self.assertTrue(new_island)

    # setters tests
    def test_set_island_grid(self):
        """
        Test to check if possible to set island grid with the landscape input
        and animals input
        """
        num_herbs = 50
        new_island = gen_island()
        grid = """W"""

        herbs_dict = [{'loc': (1, 1),
                       'pop': [{'species': 'Herbivore',
                                'age': 5,
                                'weight': 20}
                               for _ in range(num_herbs)]}]
        new_island._set_island_grid(grid, herbs_dict)

        for grid_cell in new_island._island_grid:
            cell_biome = grid_cell.cell_biome
            for herd in cell_biome.get_all_herds():
                self.assertTrue(type(cell_biome.get_landscape()) == Water and
                                len(herd.herd_list) == num_herbs)

    def test_set_neighbor_biomes(self):
        """
        Test to check if possible to set neighbouring biomes
        """
        new_island = gen_island()
        new_island._set_neighbor_biomes()

        for grid_cell in new_island._island_grid:
            cell_biome = grid_cell.cell_biome
            self.assertTrue(cell_biome._neighbor_biomes is not None)

    # getters test
    def test_get_biome(self):
        """
        Test to check if possible to get biome associated to a key
        """
        new_island = gen_island()
        key_water = (1, 1)
        biome_1_1 = new_island._get_biome(key_water)

        self.assertTrue(type(biome_1_1.get_landscape()) == Water)

    def test_get_island_grid(self):
        """
        Test check if possible to retrieve the island grid
        """
        new_island = gen_island()
        self.assertTrue(new_island._island_grid == new_island.get_island_grid())


if __name__ == '__main__':
    unittest.main()
