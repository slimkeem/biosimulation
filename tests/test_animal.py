import copy
import random
import math
import scipy.stats as stats

import unittest
import pytest

from biosim.animal import Herbivore, Carnivore

ALPHA = 0.01
SEED = 12345


# noinspection DuplicatedCode
@pytest.fixture
def set_herb_params(request):
    """
    Fixture setting class parameters on Animal Herbivore
    """
    Herbivore.set_params(request.param)
    yield
    default_params = copy.copy(Herbivore.herbivore_default_params)
    Herbivore.set_params(default_params)


@pytest.fixture
def set_carn_params(request):
    """
    Fixture setting class parameters on Animal Herbivore
    """
    Carnivore.set_params(request.param)
    yield
    default_params = copy.copy(Carnivore.carnivore_default_params)
    Carnivore.set_params(default_params)


@pytest.mark.parametrize('set_herb_params', [{'zeta': 0}], indirect=True)
def test_prob_birth(set_herb_params):
    """
    Test for making sure that the probability when giving birth is not 0.
    Setting parameter zeta to 0, making sure probability of birth is returned.
    """
    neighbor_num = 2
    age = 1
    new_herb = Herbivore(age=age)
    check_prob_birth = new_herb._calculate_probability_birth(neighbor_num=neighbor_num)
    assert check_prob_birth > 0


@pytest.mark.parametrize('set_herb_params', [{'xi': 0}], indirect=True)
def test_check_weight_after_birth(set_herb_params):
    """
    This test checks that the weight of a child is returned when birthing takes place.
    Setting parameter xi to 0, making sure weight of a child is returned.
    """
    new_herb = Herbivore()
    assert new_herb._check_weight_after_birth() is not None


@pytest.mark.parametrize('set_herb_params', [{'zeta': 0}], indirect=True)
def test_give_birth(set_herb_params, mocker):
    """
    This test checks if an animal gives birth
    Setting zeta to 0, making sure the probability of birth is returned.
    Setting the random.random returned value to 0 with mocker making sure an animal gives birth.
    """
    new_herb = Herbivore()
    mocker.patch('random.random', return_value=0)
    mocker.patch('biosim.animal.Herbivore._check_weight_after_birth', return_value=0)
    assert new_herb.give_birth(neighbor_num=2) is not None


def test_herbivore_die(mocker):
    """
    Test for checking the death of an animal is true when the returned value for mocker
    is 0.
    """
    new_herb = Herbivore()
    mocker.patch('random.random', return_value=0)
    assert new_herb.check_death() is True


@pytest.mark.parametrize('set_herb_params', [{'omega': 0}], indirect=True)
def test_herbivore_alive(set_herb_params):
    """
    Test for making sure an animal stays alive by setting the parameter omega to 0,
    by that setting the death probability to 0.
    """
    new_herb = Herbivore()
    assert new_herb.check_death() is False


def test_herbivore_move(mocker):
    """
    Test for making sure an animal is able to  migrate.
    Setting random.random returned value to 0, making sure the migration is set to True.
    By confirming that the probability of migration is greater than the random.random value.
    """
    new_herb = Herbivore()
    mocker.patch('random.random', return_value=0)
    assert new_herb.check_migration() is True


@pytest.mark.parametrize('set_herb_params', [{'mu': 0}], indirect=True)
def test_herbivore_stay(set_herb_params):
    """
    Test for making sure that an animal stays when it is not supposed to migrate.
    By setting the parameter mu to 0, the probability of migration is always 0.
    """
    new_herb = Herbivore()
    assert new_herb.check_migration() is False


def test_herbivore_give_birth_few_neighbors(mocker):
    """
    Test for checking that animal cannot give birth when the number of animals
    in the area are less than 2.
    """
    mocker.patch('random.random', return_value=0)
    neighbor_num = 1
    age = 1
    new_herb = Herbivore(age=age)
    assert new_herb.give_birth(neighbor_num) is None


def test_Z_test_migration():
    n_total = 100
    random.seed(SEED)

    p_total = 100
    probability = 0
    for _ in range(p_total):
        weight = random.randint(5, 50)
        age = random.randint(0, 50)
        prob = Herbivore.get_params()['mu'] * Herbivore(age=age, weight=weight).get_fitness()
        probability += prob/p_total

    def check_probability_migration():
        if random.random() < probability:
            return True
        return False

    n = sum(check_probability_migration() for _ in range(n_total))  # True == 1, False == 0

    mean = n_total * probability
    var = mean * (1 - probability)
    z_num = (n - mean) / math.sqrt(var)
    phi = 2 * stats.norm.cdf(-abs(z_num))
    assert phi > ALPHA


def test_gaussian_weight_distribution():
    num_herbs = 500
    herbivores = [Herbivore() for _ in range(num_herbs)]
    herb_weights = [herb.get_weight() for herb in herbivores]

    herb_stat, herb_p = stats.normaltest(herb_weights)
    assert herb_p > ALPHA


def test_carn_hunt_probability_success():
    """
    Test for checking if a carnivore hunts successfully when the herbivore
    weight is 0.
    """
    herb_fitness = 0
    new_carn = Carnivore()
    assert new_carn._calculate_hunt_success_probability(herb_fitness) > 0


def test_carn_hunt(mocker):
    """
    Test to check if a hunt is successfull when the probability of hunt
    is greater than the random value provided by random.random by
    setting the random value to 0 using mocker.
    """
    herb_fitness = 0
    mocker.patch('random.random', return_value=0)
    new_carn = Carnivore()
    assert new_carn.hunt(herb_fitness) is True


def test_carn_feed(mocker):
    """
    Test to check if a carnivore feeds on the herbivore it has hunted and by this gains
    weight.
    """
    herb_weight = 20
    mocker.patch('random.random', return_value=0)
    new_carn = Carnivore()
    old_fitness = new_carn.get_fitness()
    old_weight = new_carn.get_weight()
    new_carn.eat(herb_weight)
    assert new_carn.get_weight() > old_weight and new_carn.get_fitness() > old_fitness


class HerbivoreTestCase(unittest.TestCase):
    @pytest.fixture(autouse=True)
    def create_adult_herbivore(self):
        """
        Test for a herbivore with given weight and age
        """
        self.weight = 40
        self.age = 5
        self.adult_herbivore = Herbivore(self.weight, self.age)
        self.baby_herbivore = Herbivore()
        self.heavy_herbivore = Herbivore(weight=50)

    # constructor tests
    def test_new_herbivore_init_age(self):
        """
        Test for a new instance of an animal
        """
        self.assertTrue(self.baby_herbivore)

    def test_new_herbivore_init_weight(self):
        self.assertTrue(self.heavy_herbivore)

    def test_new_herbivore_init_age_weight(self):
        self.assertTrue(self.adult_herbivore)

    def test_new_herbivore_non_negative_fitness(self):
        """
        Test for checking that the fitness of an new animal is greater than 0
        """
        self.assertTrue(self.baby_herbivore.get_fitness() > 0)

    # setters tests
    def test_herbivore_check_age(self):
        """
        Test for checking age
        """
        self.assertTrue(self.adult_herbivore._age == self.age)

    def test_herbivore_wrong_age(self):
        """
        Test for correct age
        """
        self.assertFalse(self.adult_herbivore._age != self.age)

    def test_herbivore_set_age(self):
        """
        Test for checking if able to set age
        """
        new_herb = Herbivore()
        new_herb._set_age(self.age * 2)
        self.assertTrue(new_herb._age == self.age * 2)

    def test_herbivore_set_negative_age(self):
        """
        Test for making sure that an invalid age input raises ValueError
        """
        with self.assertRaises(ValueError):
            new_herb = Herbivore()
            new_herb._set_age(self.age * -2)

    def test_herbivore_check_weight(self):
        """
        Test for cheking weight
        """
        self.assertTrue(self.adult_herbivore._weight == self.weight)

    def test_herbivore_wrong_weight(self):
        """
        Test for correct weight
        """
        self.assertFalse(self.adult_herbivore._weight != self.weight)

    def test_herbivore_set_weight(self):
        """
        Test for if able to set weight
        """
        new_herb = Herbivore()
        new_herb._set_weight(self.weight * 2)
        self.assertTrue(new_herb._weight == self.weight * 2)

    def test_herbivore_set_negative_weight(self):
        """
        Test making sure invalid Weight input raises ValueError
        """
        with self.assertRaises(ValueError):
            new_herb = Herbivore()
            new_herb._set_weight(self.weight * -2)

    # private functions
    def test_herbivore_increase_age(self):
        """
        Test for making sure age increases
        """
        new_herb = Herbivore(age=self.age)
        new_herb._increase_age()
        self.assertTrue(new_herb._age == self.age + 1)

    def test_herbivore_increase_weight(self):
        """
        Test for making sure weight increases
        """
        new_herb = Herbivore(weight=self.weight)
        new_herb._increase_weight()
        self.assertTrue(new_herb._weight > self.weight)

    def test_herbivore_decrease_weight_birth(self):
        """
        Test for making sure weight gets reduced
        """
        new_herb = Herbivore(weight=self.weight)
        new_herb._decrease_weight(child_weight=self.weight*0.9)
        self.assertTrue(new_herb._weight < self.weight)

    def test_herbivore_decrease_weight_normal(self):
        """
        Test for making sure the normal weight gets reduced
        when weight is not passed
        """
        new_herb = Herbivore(weight=self.weight)
        new_herb._decrease_weight()
        self.assertTrue(new_herb._weight < self.weight)

    # Public functions
    def test_herbivore_update_yearly_age(self):
        """
        Test making sure age increases for one cycle
        """
        new_herb = Herbivore(age=self.age)
        new_herb.update_yearly_age()
        self.assertTrue(new_herb._age == self.age + 1)

    def test_herbivore_update_yearly_weight(self):
        """
        Test making sure weight updates
        """
        new_herb = Herbivore(weight=self.weight)
        new_herb.update_yearly_weight()
        self.assertTrue(new_herb._weight < self.weight)

    def test_herbivore_feed(self):
        """
        Test making sure Herbivores feed gaining weight
        """
        new_herb = Herbivore(weight=self.weight)
        new_herb.eat()
        self.assertTrue(new_herb._weight >= self.weight)

    def test_herbivore_die_weight_zero(self):
        """
        Test to check if an animal dies when weight is 0
        """
        new_herb = Herbivore()
        new_herb._set_weight(0)
        self.assertTrue(new_herb.check_death())


if __name__ == '__main__':
    unittest.main()
