import copy
import unittest
import pytest

from biosim.biome import Herd, Biome
from biosim.animal import Herbivore, Carnivore
from biosim.landscape import Water, Lowland, Desert


def new_biome():
    all_herds = [new_herb_herd(), new_carn_herd()]
    low_biome = Biome(Lowland(), all_herds)
    return low_biome


def new_herb_herd():
    num_herbs = 10
    herbivores = [Herbivore(age=3) for _ in range(num_herbs)]
    new_herbivore_herd = Herd(is_predator=False, herd_list=herbivores)
    return new_herbivore_herd


def new_carn_herd():
    num_carns = 2
    carnivores = [Carnivore(age=3) for _ in range(num_carns)]
    new_carnivore_herd = Herd(is_predator=True, herd_list=carnivores)
    return new_carnivore_herd


@pytest.fixture
def set_carn_params(request):
    Carnivore.set_params(request.param)
    yield
    default_params = copy.copy(Carnivore.carnivore_default_params)
    Carnivore.set_params(default_params)


@pytest.fixture
def set_herb_params(request):
    Herbivore.set_params(request.param)
    yield
    default_params = copy.copy(Herbivore.herbivore_default_params)
    Herbivore.set_params(default_params)


@pytest.mark.parametrize('set_herb_params', [{'xi': 0}], indirect=True)
@pytest.mark.parametrize('set_carn_params', [{'xi': 0}], indirect=True)
def test_procreate(set_herb_params, set_carn_params, mocker):
    """
    Test to check if the procreation cycle works by setting param xi to 0 and
    using mocker to set the probability of birth to one making sure animals give birth
    and then comparing the old population with the new one to check if the
    number of animals increased.
    """
    fertile_biome = new_biome()
    fertile_herds = fertile_biome.get_all_herds()
    old_herb_population = 0
    new_herb_population = 0
    old_carn_population = 0
    new_carn_population = 0
    for specie in fertile_herds:
        if specie.is_predator is True:
            old_carn_population = len(specie.herd_list)
        if specie.is_predator is False:
            old_herb_population = len(specie.herd_list)
    mocker.patch('random.random', return_value=0)
    mocker.patch('biosim.animal.Animal._calculate_probability_birth',
                 return_value=1)
    fertile_biome.procreate()
    for specie in fertile_herds:
        if specie.is_predator is True:
            new_carn_population = len(specie.herd_list)
        if specie.is_predator is False:
            new_herb_population = len(specie.herd_list)
    assert (old_herb_population < new_herb_population and
            old_carn_population < new_carn_population)


def test_carnivores_feed(mocker):
    """
    Test for carnivores feeding cycle in biome using mocker to set
    the carnivore hunt to True by this making sure carnivores hunt.
    Testing if the weight of the carnivores increase and the population
    of herbivores decreases.
    """

    dangerous_biome = new_biome()
    predator_herd = dangerous_biome.get_all_herds()
    old_weight = []
    old_prey_population = 0
    new_prey_population = 0
    for specie in predator_herd:
        if specie.is_predator is True:
            for anim in specie.herd_list:
                old_weight.append(anim.get_weight())
        if specie.is_predator is False:
            old_prey_population = len(specie.herd_list)
    mocker.patch('biosim.animal.Carnivore.hunt', return_value=True)
    dangerous_biome.feed()
    new_weight = []
    for specie in predator_herd:
        if specie.is_predator is True:
            for anim in specie.herd_list:
                new_weight.append(anim.get_weight())
        if specie.is_predator is False:
            new_prey_population = len(specie.herd_list)
    for i, _ in enumerate(new_weight):
        for _ in enumerate(old_weight):
            assert new_weight[i] > old_weight[i] and \
                   old_prey_population > new_prey_population


def test_die(mocker):
    """
    Test for death cycle in biome by using mocker to set the death
    in animals to True. Making sure animals die and comparing the lengths
    of the lists before and after death cycle.
    """
    dying_biome = new_biome()
    initial_herd = dying_biome.get_all_herds()
    ini_herbivore_pop_n = 0
    ini_carnivore_pop_n = 0
    final_herbivore_pop_n = 0
    final_carnivore_pop_n = 0
    for specie in initial_herd:
        if specie.is_predator is False:
            ini_herbivore_pop_n = len(specie.herd_list)
        if specie.is_predator is True:
            ini_carnivore_pop_n = len(specie.herd_list)
    mocker.patch('biosim.animal.Animal.check_death', return_value=True)
    dying_biome.die()
    for specie in initial_herd:
        if specie.is_predator is False:
            final_herbivore_pop_n = len(specie.herd_list)
        if specie.is_predator is True:
            final_carnivore_pop_n = len(specie.herd_list)
    assert (ini_herbivore_pop_n > final_herbivore_pop_n and
            ini_carnivore_pop_n > final_carnivore_pop_n)


def test_emigrate(mocker):
    """
    Test to check if animals moves from one cell to another by using mocker
    to make sure every animals moves and then making sure all animals left to another cell
    """
    moving_biome = new_biome()
    neighbor_biome_a = new_biome()
    neighbor_biome_b = new_biome()
    neighbor_biome_c = new_biome()
    neighbor_biome_d = new_biome()
    moving_biome.set_neighbors([neighbor_biome_a, neighbor_biome_b,
                                neighbor_biome_c, neighbor_biome_d])
    initial_herd = moving_biome.get_all_herds()
    ini_herbivore_pop_n = 0
    ini_carnivore_pop_n = 0
    final_herbivore_pop_n = 0
    final_carnivore_pop_n = 0
    for specie in initial_herd:
        if specie.is_predator is False:
            ini_herbivore_pop_n = len(specie.herd_list)
        if specie.is_predator is True:
            ini_carnivore_pop_n = len(specie.herd_list)
    mocker.patch('biosim.animal.Animal.check_migration', return_value=True)
    move_list = moving_biome.emigrate()
    for specie in initial_herd:
        if specie.is_predator is False:
            final_herbivore_pop_n = len(specie.herd_list)
        if specie.is_predator is True:
            final_carnivore_pop_n = len(specie.herd_list)
    assert (len(list(move_list)) == ini_herbivore_pop_n + ini_carnivore_pop_n and
            final_herbivore_pop_n + final_carnivore_pop_n == 0)


def test_immigrate():
    """
    Test to check if animals immigrate properly and that the population
    increases with animals moving in.
    """
    ini_herbivore_pop_n = 0
    ini_carnivore_pop_n = 0
    final_herbivore_pop_n = 0
    final_carnivore_pop_n = 0
    immig_biome = new_biome()
    immigrant1 = Herbivore()
    immigrant2 = Carnivore()
    for specie in immig_biome.get_all_herds():
        if specie.is_predator is False:
            ini_herbivore_pop_n = len(specie.herd_list)
        if specie.is_predator is True:
            ini_carnivore_pop_n = len(specie.herd_list)
    immig_biome.immigrate(immigrant1)
    immig_biome.immigrate(immigrant2)
    for specie in immig_biome.get_all_herds():
        if specie.is_predator is False:
            final_herbivore_pop_n = len(specie.herd_list)
        if specie.is_predator is True:
            final_carnivore_pop_n = len(specie.herd_list)
    assert (ini_herbivore_pop_n < final_herbivore_pop_n and
            ini_carnivore_pop_n < final_carnivore_pop_n)


class BiomeTestCase(unittest.TestCase):
    # Constructors
    def test_new_herd(self):
        """
        Test to check new herd
        """
        num_herbs = 10
        herbivores = [Herbivore(age=3) for _ in range(num_herbs)]
        new_herbivore_herd = Herd(is_predator=False, herd_list=herbivores)
        self.assertTrue(new_herbivore_herd)

    def test_empty_herd(self):
        """
        Test for empty herd
        """
        empty_herd = Herd(is_predator=True)
        self.assertTrue(empty_herd)

    def test_new_landscape(self):
        """
        Test new landscape
        """
        lowland_landscape = Lowland()
        self.assertTrue(lowland_landscape)

    def test_new_biome(self):
        """
        Test new biome instance
        """
        self.assertTrue(new_biome())

    def test_empty_biome(self):
        """
        Test empty biome containing water
        """

        empty_biome = Biome(Water())
        self.assertTrue(empty_biome)

    # setters tests
    def test_set_herbivore_herds(self):
        """
        Test to check if possible to set herd of animals
        """

        empty_biome = Biome(Desert())
        herb_herd = new_herb_herd()

        empty_biome._set_all_herds([herb_herd])
        self.assertTrue(empty_biome.get_all_herds() == [herb_herd])

    def test_set_multiple_herds(self):
        """
        Test to check if possible to set a herd with both animal types
        """
        empty_biome = Biome(Desert())
        herb_herd = new_herb_herd()
        carn_herd = new_carn_herd()
        empty_biome._set_all_herds([herb_herd, carn_herd])
        self.assertTrue(empty_biome.get_all_herds() == [herb_herd, carn_herd])

    # public functions
    def test_set_neighbors(self):
        """
        Test to check if possible to set neighbouring biomes
        """
        fresh_biome = new_biome()
        neighbor_biome = new_biome()
        fresh_biome.set_neighbors(neighbor_biome)
        self.assertTrue(fresh_biome._neighbor_biomes is not None)

    def test_replenish_fodder(self):
        """
        Test for biome fodder updates each cycle
        """
        fod_biome = new_biome()
        eaten_fodder = 200
        fod_biome.get_landscape().reduce_eaten_fodder(eaten_fodder)
        max_fodder = fod_biome.get_landscape().get_params()['f_max']
        fod_biome.replenish_fodder()
        new_fodder = fod_biome.get_landscape().get_current_fodder()
        self.assertTrue(new_fodder == max_fodder)

    def test_feed_herbivore(self):
        """
        Test to check if herbivores feed the amount of fodder reduces
        """
        feed_biome = new_biome()
        max_fodder = feed_biome.get_landscape().get_params()['f_max']
        feed_biome.feed()
        reduced_fodder = feed_biome.get_landscape().get_current_fodder()
        self.assertTrue(reduced_fodder < max_fodder)

    def test_feed_herbivore_weight(self):
        """
        Test to check if weight of herbivores increases when they eat F amounts of
        fodder
        """
        herb_biome = new_biome()
        herb_herd = herb_biome.get_all_herds()
        old_weight = []
        for specie in herb_herd:
            if specie.is_predator is False:
                for anim in specie.herd_list:
                    old_weight.append(anim.get_weight())
        herb_biome.feed()
        new_weight = []
        for specie in herb_herd:
            if specie.is_predator is False:
                for anim in specie.herd_list:
                    new_weight.append(anim.get_weight())
        for i, _ in enumerate(new_weight):
            for _ in enumerate(old_weight):
                self.assertTrue(new_weight[i] > old_weight[i])

    def test_migration_status(self):
        """
        Test to check if the migration status of each animal in the herd
        resets to False each cycle
        """
        mig_biome = new_biome()
        current_herd_list = mig_biome.get_all_herds()
        for specie in current_herd_list:
            for anim in specie.herd_list:
                anim._just_moved = True
        mig_biome.reset_yearly_migration_status()
        for specie in current_herd_list:
            for anim in specie.herd_list:
                self.assertFalse(anim.get_just_moved_status())

    def test_age(self):
        """
        Test to check if age increases for each animal in herd for each cycle
        """
        old_biome = new_biome()
        old_herd = old_biome.get_all_herds()
        old_ages = []
        for specie in old_herd:
            for anim in specie.herd_list:
                old_ages.append(anim.get_age())
        old_biome.age()
        current_herd_list = old_biome.get_all_herds()
        age_list = []
        for specie in current_herd_list:
            for anim in specie.herd_list:
                age_list.append(anim.get_age())
        for i, _ in enumerate(age_list):
            for _ in enumerate(old_ages):
                self.assertTrue(age_list[i] > old_ages[i])

    def test_yearly_weight_loss(self):
        """
        Test to check if each animal in herd loses weight each cycle
        """
        wei_biome = new_biome()
        old_herd = wei_biome.get_all_herds()
        old_weight = []
        for specie in old_herd:
            for anim in specie.herd_list:
                old_weight.append(anim.get_weight())
        wei_biome.lose_yearly_weight()
        current_herd = wei_biome.get_all_herds()
        new_weight = []
        for specie in current_herd:
            for anim in specie.herd_list:
                new_weight.append(anim.get_weight())
        for i, _ in enumerate(new_weight):
            for _ in enumerate(old_weight):
                self.assertTrue(new_weight[i] < old_weight[i])

    def test_get_herd(self):
        """
        Test to check if possible to get current herd list in biome
        """
        all_herds = [new_herb_herd()]
        curr_land = Lowland()
        her_biome = Biome(curr_land, all_herds)
        current_herd = her_biome.get_all_herds()
        self.assertTrue(current_herd == all_herds)

    def test_get_landscape(self):
        """
        Test to check if possible to get current landscape in biome
        """
        curr_land = Lowland()
        lan_biome = Biome(curr_land)
        self.assertTrue(lan_biome.get_landscape() == curr_land)


if __name__ == '__main__':
    unittest.main()
