import unittest

from biosim.landscape import Lowland, Water, Desert, Highland

lowland = Lowland()
fodder = 800
fodder_2 = 300
water = Water()
desert = Desert()
highland = Highland()


class LandscapeTestCase(unittest.TestCase):

    def test_water(self):
        """
        Test if a single water instance is created
        """
        self.assertTrue(water)

    def test_lowland(self):
        """
        Test if a single lowland instance is created
        """
        self.assertTrue(lowland)

    def test_desert(self):
        """
        Test if single desert instance is created
        """
        self.assertTrue(desert)

    def test_highland(self):
        """
        Test if single highland instance is created
        """
        self.assertTrue(highland)

    def test_set_fodder_lowland(self):
        """
        Test to check if fodder sets for lowland
        """
        new_lowland = Lowland()
        new_lowland._set_fodder()
        self.assertTrue(new_lowland.get_current_fodder() == fodder)

    def test_set_fodder_highland(self):
        """
        Test to check if fodder sets for highland
        """
        new_highland = Highland()
        new_highland._set_fodder()
        self.assertTrue(new_highland.get_current_fodder() == fodder_2)

    def test_reduce_fodder_lowland(self):
        """
        Test to check if fodder gets reduced when eaten lowland
        """
        new_lowland = Lowland()
        new_lowland.reduce_eaten_fodder(200)
        self.assertTrue(new_lowland._current_fodder == fodder - 200)

    def test_reduce_fodder_highland(self):
        """
        Test to check if fodder gets reduced when eaten highland
        """
        new_highland = Highland()
        new_highland.reduce_eaten_fodder(10)
        self.assertTrue(new_highland._current_fodder == fodder_2 - 10)

    def test_fertility_lowland(self):
        """
        Test to check if landscape is fertile, True in the case for lowland
        """
        new_lowland = Lowland()
        self.assertTrue(new_lowland.get_fertility_check())

    def test_fertility_desert(self):
        """
        Test to check if landscape is fertile, False for desert
        """
        new_desert = Desert()
        self.assertFalse(new_desert.get_fertility_check())

    def test_get_habitability(self):
        """
        Test to check if a landscape type is habitable or not,
        False in the case for Water
        """
        new_water = Water()
        self.assertFalse(new_water.get_habitability())

    def test_get_current_fodder(self):
        """
        Test to check if able to get the current fodder
        """
        new_lowland = Lowland()
        self.assertTrue(new_lowland.get_current_fodder() == fodder)

    def set_params_landscape(self):
        """
        Test to check if able to set params for a landscape, mainly 'f_max'
        """
        new_lowland = Lowland()
        new_lowland.set_params({'f_max': 100})
        self.assertTrue(new_lowland.get_current_fodder() == 100)

    def test_update_yearly_fodder(self):
        """
        Test to check if the fodder resets yearly
        """
        new_lowland = Lowland()
        new_lowland.reduce_eaten_fodder(200)
        new_lowland.update_yearly_fodder()
        self.assertTrue(new_lowland.get_current_fodder() == fodder)

    def test_get_params(self):
        """
        Test to check if able to get the params for a landscape
        """
        new_highland = Highland()
        params_ = new_highland.get_params()
        self.assertTrue(params_ == {'f_max': 300})


if __name__ == '__main__':
    unittest.main()
