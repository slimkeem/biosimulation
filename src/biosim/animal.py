# -*- codingK utf-8 -*-

"""
Python package for biosim
"""

__author__ = 'AbdulHakeem Ayodele Adamu, Atif Fazal'
__email__ = 'abdulhakeem.ayodele.adamu@nmbu.no, atif.fazal@nmbu.no'

import copy
import random
import numpy as np
from math import exp

"""
Implements a model for animals and its subclasses
"""


class Animal:
    """
    Animals which age, gain weight, lose weight, procreate, migrate and die.
    """

    params = {}

    @classmethod
    def set_params(cls, param_dict):
        """
        Sets class parameters.

        Parameters
        ----------
        param_dict : dict
            keys : 'w_birth', 'sigma_birth', 'beta', 'eta', 'a_half', 'phi_age',
                  'w_half','phi_weight', 'mu', 'gamma', 'zeta', 'xi', 'omega',
                  'F', 'DeltaPhiMax'
        """

        cls.params.update(param_dict)

    @classmethod
    def get_params(cls):
        """
        Gets the class parameters

        Returns
        -------
        dict
            Dictionary with the class parameters
        """

        return cls.params

    def __init__(self, weight=None, age=None):
        """
        Initializes the Animal (base) class.

        Parameters
        ----------
        weight : float [default=none]
            weight of an animal

        age : int [default=None]
            age of an animal
        """

        self._set_age(age)
        self._set_weight(weight)
        self._calculate_fitness()
        self._just_moved = False
        self._is_predator = None

    # Private functions
    # VOID
    def _set_age(self, age):
        """
        Sets the age of an animal by age. Otherwise set to 0

        Parameters
        ----------
        age : int
            age of an animal that must be an integer and not lower than 0

        Raises
        ------
        ValuesError
        """

        if age is not None:
            if age < 0:
                raise ValueError("Age cannot be lower than 0")
            elif type(age) != int:
                raise ValueError("Age must be an integer")
            elif age >= 0:
                self._age = age
        else:
            self._age = 0

    def _increase_age(self):
        """Increase age by one"""

        self._age += 1

    def _set_weight(self, weight):
        """
        Sets the weight of an animal by weight.
        Otherwise weight is gotten from _calculate_normal_weight

        Parameters
        ----------
        weight : float
            weight must be float and cannot be lower than 0

        Raises
        ------
        ValueError
        """

        if weight is not None:
            if weight >= 0:
                self._weight = weight
            else:
                raise ValueError("Weight must be greater than 0")
        else:
            self._weight = self._calculate_normal_weight()

    def _increase_weight(self, herb_weight=None):
        r"""
        Calculates the weight gain of animals.
        For herbivores:
        :math:`\beta\mathrm{F}`

        For carnivores:
        :math:`\beta \times herb_weight`

        If herb_wight is not none - overloads the function to calculate
        weight gain for carnivores with herb_weight.

        Parameters
        ---------
        herb_weight : float
            [default=none]
            weight of a herbivore that a carnivore has eaten.
        """

        if herb_weight is not None:
            self._weight += self.params['beta'] * herb_weight
        else:
            self._weight += self.params['beta'] * self.params['F']

    def _decrease_weight(self, child_weight=None):
        """
        Decreases the weight of an animal.

        .. math::
            ...

        If child_weight is not None, decrease weight by child_weight

        .. math::
            ...

        Parameters
        ---------
        child_weight : float
            [default=none]
            weight of the child an animal has given birth to
        """

        if child_weight is not None:
            self._weight -= self.params['xi'] * child_weight
        else:
            self._weight -= self.params['eta'] * self._weight

    def _calculate_fitness(self):
        r"""
        Calculates the fitness of an animal.

        :math:`\textit{q}^+(\textit{a},\textit{a}_{\frac{1}{2}},\Phi) \times
        \textit{q}^-(\textit{a},\textit{a}_{\frac{1}{2}},\Phi)`

        where :math:`\textit{q}^\pm(\textit{a},\textit{a}_{\frac{1}{2}},\Phi) =
        \frac{1}{1 + e^{\pm\phi(\textit{x}-\textit{x}_{\frac{1}{2}})}}`
        """

        if self._weight == 0:
            self._fitness = 0
        else:
            self._fitness = (1 / (1 + exp((1 * self.params['phi_age']) *
                                          (self._age - self.params['a_half'])))) * \
                            (1 / (1 + exp((-1 * self.params['phi_weight']) *
                                          (self._weight - self.params['w_half']))))

    # NON-VOID
    def _calculate_normal_weight(self):
        r"""
        Calculates the weight of an animal drawn from a normal Gaussian distribution

        :math:`\textit{w} \sim \mathcal{N}(\textit{w}_{birth}, \sigma_{birth})`

        Returns
        -------
        float
            weight of a new animal
        """

        # normal_weight = random.gauss(self.params['w_birth'], self.params['sigma_birth'])
        normal_weight = np.random.normal(self.params['w_birth'], self.params['sigma_birth'])
        return normal_weight

    def _calculate_probability_birth(self, neighbor_num):
        r"""
        Calculates the probability of an animal giving birth.
        If there are less than 2 animals of the same species the probability is set to 0.
        If the weight is lower than a certain threshold the probability is also set to 0 .

        Probability of birth
        :math:`\min(1, \gamma \times \Phi \times (N -1))`

        Parameters
        ---------
        neighbor_num : int
            number of animals of the same species in a cell

        Returns
        -------
        float
            the probability of an animal giving birth.
        """

        if neighbor_num <= 1:
            prob_birth = 0
        elif self._weight < self.params['zeta'] * (self.params['w_birth'] +
                                                   self.params['sigma_birth']):
            prob_birth = 0
        else:
            prob_birth = min(1, self.params['gamma'] * self._fitness * (neighbor_num - 1))
        return prob_birth

    def _check_weight_after_birth(self):
        r"""
        Checks if the weight of a child will be equal or more than that of the mother.

        Weight of new child:
        :math:`\textit{w} \sim \mathcal{N}(\textit{w}_{birth}, \sigma_{birth})`


        Returns
        ---------
        float
            the weight of the new animal
            None: if no new child
        """

        child_weight = self._calculate_normal_weight()
        if (self.params['xi'] * child_weight) <= self._weight:
            return child_weight
        else:
            return None

    # Public functions
    # VOID
    def update_yearly_age(self):
        r"""
        Updates the age of animals by one each cycle and calculate its fitness

        Fitness equation:
        :math:`\textit{q}^+(\textit{a},\textit{a}_{\frac{1}{2}},\Phi) \times
        \textit{q}^-(\textit{a},
        \textit{a}_{\frac{1}{2}},\Phi)`

        where :math:`\textit{q}^\pm(\textit{a},\textit{a}_{\frac{1}{2}},\Phi) =
        \frac{1}{1 + e^{\pm\phi(\textit{x}-\textit{x}_{\frac{1}{2}})}}`

        note that :math:`0\leq\phi\leq1`
        """

        self._increase_age()
        self._calculate_fitness()

    def update_yearly_weight(self):
        r"""
        Updates the weight of an animal each cycle and calculate its fitness

        Weight decrease equation:
        :math:`\eta\textit{w}`

        Fitness equation:
        :math:`\textit{q}^+(\textit{a},\textit{a}_{\frac{1}{2}},\Phi) \times
        \textit{q}^-(\textit{a},
        \textit{a}_{\frac{1}{2}},\Phi)`

        where :math:`\textit{q}^\pm(\textit{a},\textit{a}_{\frac{1}{2}},\Phi) =
        \frac{1}{1 + e^{\pm\phi(\textit{x}-\textit{x}_{\frac{1}{2}})}}`

        note that :math:`0\leq\phi\leq1`
        """

        self._decrease_weight()
        self._calculate_fitness()

    def eat(self, herb_weight=None):
        r"""
        updates the weight of animal based on what the animal eats.
        If herb_weight is None, overload the function to work for carnivores.

        Weight increase for herbivores:
        :math:`\beta\mathrm{F}`

        For carnivores:
        :math:`\beta \times` w_herb

        Parameters
        ---------
        herb_weight : float
        """

        self._increase_weight(herb_weight)
        self._calculate_fitness()

    def change_move_status(self, val):
        """
        Change the migration status of an animal whenever it has moved, or yearly.

        Parameters
        ---------
        val : bool
        """

        self._just_moved = val

    # NON-VOID
    def get_age(self):
        """
        Get the age of an animal

        Returns
        ------
        int
            age
        """
        return self._age

    def get_weight(self):
        """
        Get the weight of an animal

        Returns
        -------
        float
            weight
        """

        return self._weight

    def get_fitness(self):
        """
        Get the fitness of an animal

        Returns
        ------
        float
            fitness
        """

        return self._fitness

    def give_birth(self, neighbor_num):
        r"""
        Checks if an animal will give birth with probaility:

        zero if no animals nearby

        also zero if :math:`\textit{w} < \zeta(\textit{w}_{birth} + \sigma_{birth})`

        otherwise probability of birth:
        :math:`\min(1, \gamma \times \Phi \times (N -1))`

        Weight of new child:
        :math:`\textit{w} \sim \mathcal{N}(\textit{w}_{birth}, \sigma_{birth})`

        Parameters
        ---------
        neighbor_num : int

        Returns
        -------
        bool
            true if the animal will give birth, false otherwise
        """
        if random.random() < self._calculate_probability_birth(neighbor_num):
            child_weight = self._check_weight_after_birth()
            if child_weight is not None:
                self._decrease_weight(child_weight)
                self._calculate_fitness()
                return type(self)(weight=child_weight)
            else:
                return None
        else:
            return None

    def check_death(self):
        r"""
        Calculates the probability that an animal has died.
        An animal dies with certainty if its weight is less than or equal to 0

        Probability of death:

        zero if :math:`\textit{w} = 0`

        otherwise with probability

        :math:`\omega(1 - \Phi)`

        Returns
        ------
        bool
            True if animal dies
            False otherwise
        """

        if self._weight <= 0:
            return True
        else:
            death_prob = self.params['omega'] * (1 - self._fitness)
            return random.random() < death_prob

    def check_migration(self):
        r"""
        Checks if an animal will try to migrate with probability:
        :math:`\mu\Phi`

        Returns
        -------
        bool
            true if the animal will try and migrate
        """

        migration_prob = self.params['mu'] * self._fitness
        if random.random() < migration_prob:
            return True
        else:
            return False

    def get_just_moved_status(self):
        """
        Returns migration status of an animal

        Returns
        ------
        bool
            _just_moved
        """

        return self._just_moved

    def isPredator(self):
        """ Checks if an animal is a predator or not """
        return self._is_predator


class Herbivore(Animal):
    """ Herbivore: Subclass of Animal"""

    herbivore_default_params = {
        'w_birth': 8.0,
        'sigma_birth': 1.5,
        'beta': 0.9,
        'eta': 0.05,
        'a_half': 40.0,
        'phi_age': 0.6,
        'w_half': 10.0,
        'phi_weight': 0.1,
        'mu': 0.25,
        'gamma': 0.2,
        'zeta': 3.5,
        'xi': 1.2,
        'omega': 0.4,
        'F': 10.0
    }

    params = copy.copy(herbivore_default_params)

    def __init__(self, weight=None, age=None):
        """
        Initializes the Herbivore (sub) class by running init of parent class

        Parameters
        ----------
        weight : float
            [default=none]
            weight of an animal

        age : int
            [default=None]
            age of an animal
        """

        super(Herbivore, self).__init__(weight, age)
        self._is_predator = False


class Carnivore(Animal):
    """ Carnivore: Subclass of Animal"""

    carnivore_default_params = {
        'w_birth': 6.0,
        'sigma_birth': 1.0,
        'beta': 0.75,
        'eta': 0.125,
        'a_half': 40.0,
        'phi_age': 0.3,
        'w_half': 4.0,
        'phi_weight': 0.4,
        'mu': 0.4,
        'gamma': 0.8,
        'zeta': 3.5,
        'xi': 1.1,
        'omega': 0.8,
        'F': 50.0,
        'DeltaPhiMax': 10.0
    }

    params = copy.copy(carnivore_default_params)

    def __init__(self, weight=None, age=None):
        """
        Initializes the Carnivore (sub) class by running init of parent class

        Parameters
        ----------
        weight : float
            [default=none]
            weight of an animal

        age : int
            [default=None]
            age of an animal
        """

        super(Carnivore, self).__init__(weight, age)
        self._is_predator = True

    # Private Function
    # NON-VOID
    def _calculate_hunt_success_probability(self, herb_fitness):
        """
        Calculate the probability of a successful hunt.
        If the fitness of a herbivore is greater than that of the carnivore
        the probability is 0.


        Parameters
        ----------
        herb_fitness : float
            fitness of the herbivore being hunted

        Returns
        -------
        float
            probability that carnivore kills the herbivore
        """

        if herb_fitness > self._fitness:
            prob_kill = 0
        elif 0 < (self._fitness - herb_fitness) < self.params['DeltaPhiMax']:
            prob_kill = (self._fitness - herb_fitness) / self.params['DeltaPhiMax']
        else:
            prob_kill = 1
        return prob_kill

    # Public Function
    # NON-VOID
    def hunt(self, herb_fitness):
        r"""
        Decides if the hunt was successful or not.

        Probability of successful hunt:

        Zero if :math:`\Phi_{carn}\leq\Phi_{herb}`

        :math:`\frac{\Phi_{carn} - \Phi_{herb}}{\Delta\Phi}`
        if :math:`0\leq\Phi_{carn} - \Phi_{herb}\leq\Delta\Phi`.

        otherwise 1

        Parameters
        ----------
        herb_fitness : float
            Herbivore fitness

        Returns
        -------
        bool
            True if successful hunt, False otherwise
        """

        prob_kill = self._calculate_hunt_success_probability(herb_fitness)
        if random.random() < prob_kill:
            return True
        return False
