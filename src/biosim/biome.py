# -*- codingK utf-8 -*-

"""
Python package for biosim
"""

__author__ = 'AbdulHakeem Ayodele Adamu, Atif Fazal'
__email__ = 'abdulhakeem.ayodele.adamu@nmbu.no, atif.fazal@nmbu.no'

from numpy.random import permutation
from dataclasses import dataclass
import random

from biosim.landscape import Landscape

"""
Implements a model for Biome
"""


@dataclass
class Herd:
    """Herd of a group of a particular species of animals"""

    is_predator: bool
    herd_list: list = None


class Biome:
    """
    Class which contains which contains a specific landscape and the various herds of animals
    found on that landscape. Has methods which handles the interaction between the herds
    and the landscape along with annual cycle events.
    """

    def __init__(self, current_landscape: Landscape, all_herds=None):
        """
        Initializes the Biome class

        Parameters
        ----------
        current_landscape: Landscape
            specific landscape of the biome

        all_herds: list
            list containing all the herds found on the landscape
        """

        # properties
        self._set_all_herds(all_herds)
        self._set_landscape(current_landscape)
        self._neighbor_biomes = []

    # Private Functions
    # VOID
    def _set_all_herds(self, all_herds=None):
        """
        Sets the initial herd of animals in the Biome

        Parameters
        ----------
        all_herds: list
            list containing all the herds found on the landscape
        """
        if all_herds:
            self._all_herds = all_herds
        else:
            self._all_herds = []

    def _set_landscape(self, current_landscape):
        """
        Sets the Landscape of the Biome

        Parameters
        ----------
        current_landscape: Landscape
            landscape of biome
        """

        self._landscape = current_landscape

    # Public Functions
    # VOID
    def set_neighbors(self, neighbor_biomes=None):
        """
        Sets the neighbouring cells

        Parameters
        ----------
        neighbor_biomes: list
            a list of all the neighboring cells animals can migrate to
        """

        self._neighbor_biomes = neighbor_biomes

    def replenish_fodder(self):
        """Replenishes the fodder in the different landscapes"""

        if self._landscape.get_fertility_check():
            self._landscape.update_yearly_fodder()

    def feed(self):
        """
        Feeding cycle for both herbivores and carnivores.

        Herbivores feed on fodder in the landscape in a random order.

        Carnivores feed on herbivores (killing them in the process) in order of how fit
        the carnivore is and how unfit the herbivore is.
        """
        # herbivore_herd_list = self._get_herbivores()
        # carnivore_herd_list = self._get_carnivores()

        # feed herbivores
        for current_herd in self._all_herds:
            if current_herd.is_predator is False and self._landscape.get_current_fodder() > 0:
                for single_herbivore in permutation(current_herd.herd_list):
                    if single_herbivore.get_params()['F'] <= self._landscape.get_current_fodder():
                        single_herbivore.eat()
                        self._landscape.reduce_eaten_fodder(single_herbivore.get_params()['F'])

            # feed carnivores
            if current_herd.is_predator:
                for next_herd in self._all_herds:
                    if next_herd.is_predator is False:
                        next_herd.herd_list.sort(
                            key=lambda herbivore: herbivore.get_fitness())
                        current_herd.herd_list.sort(
                            key=lambda carnivore: carnivore.get_fitness(), reverse=True)
                        for single_predator in current_herd.herd_list:
                            meat_eaten = 0
                            for single_prey in next_herd.herd_list:
                                if meat_eaten < single_predator.get_params()['F']:
                                    herb_weight = single_prey.get_weight()
                                    herb_fitness = single_prey.get_fitness()
                                    hunt_success = single_predator.hunt(
                                        herb_fitness=herb_fitness)
                                    if hunt_success:
                                        if herb_weight + meat_eaten > \
                                                single_predator.get_params()['F']:
                                            single_predator.eat(single_predator.get_params()['F'] -
                                                                meat_eaten)
                                            meat_eaten = single_predator.get_params()['F']
                                        else:
                                            single_predator.eat(herb_weight)
                                            meat_eaten += herb_weight
                                        next_herd.herd_list = [single_herb for single_herb
                                                               in next_herd.herd_list
                                                               if single_herb != single_prey]
                                else:
                                    break

    def procreate(self):
        """
        Animals in the biome procreate to produce young animals. Newly born animals can not
        participate in the procreation process.
        """

        for current_herd in self._all_herds:
            adults_on_heat = [elem for elem in current_herd.herd_list
                              if elem.get_age() > 0]
            for single_animal in adults_on_heat:
                new_child = single_animal.give_birth(len(adults_on_heat))
                if new_child:
                    current_herd.herd_list.append(new_child)

    def immigrate(self, immigrating_animal):
        """
        Adds the animals that just came into a cell into the right herds, or creates a new herd
        if none is suitable.

        Parameters
        ---------
        immigrating_animal: Animal
            An animal that just came into the biome
        """
        if self._all_herds:
            present_herd = [current_herd for current_herd in self._all_herds
                            if (current_herd.is_predator == immigrating_animal.isPredator())]
            if present_herd:
                for current_herd in present_herd:
                    if current_herd.is_predator == immigrating_animal.isPredator():
                        current_herd.herd_list.append(immigrating_animal)
            else:
                self._all_herds.append(
                    Herd(is_predator=immigrating_animal.isPredator(),
                         herd_list=[immigrating_animal]))
        else:
            self._all_herds.append(
                Herd(is_predator=immigrating_animal.isPredator(),
                     herd_list=[immigrating_animal]))

    def reset_yearly_migration_status(self):
        """ Resets migration status of every animal so they may migrate again that season """

        for current_herd in self._all_herds:
            for single_animal in current_herd.herd_list:
                single_animal.change_move_status(False)

    def age(self):
        """Age increase for every animal in the Biome"""

        for current_herd in self._all_herds:
            for single_animal in current_herd.herd_list:
                single_animal.update_yearly_age()

    def lose_yearly_weight(self):
        """Weight decrease update for every animal in the Biome."""

        for current_herd in self._all_herds:
            for single_animal in current_herd.herd_list:
                single_animal.update_yearly_weight()

    def die(self):
        """Checks if an animal is going to die and then removes said animal from Biome."""

        for current_herd in self._all_herds:
            current_herd.herd_list = [single_animal for single_animal
                                      in current_herd.herd_list
                                      if single_animal.check_death()
                                      is False]

    # NON-VOID
    def emigrate(self):
        """
        Collects all migrating animals in a list. The migration of each animal
        in the biome is first confirmed. If animal has decided to move, the cell moved
        to is random between the four neighbouring cells. An animal can only move
        to the chosen cell if that cell is habitable. It then removes the migrating animals
        from the herd.

        Returns
        -------
        list
            list of animals moving
        """

        move_list = []
        for current_herd in self._all_herds:
            for single_animal in current_herd.herd_list:
                if single_animal.get_just_moved_status():
                    continue
                else:
                    if single_animal.check_migration():
                        neighbor = random.choice(self._neighbor_biomes)
                        neighbor_landscape = neighbor.get_landscape()
                        if neighbor_landscape.get_habitability():
                            single_animal.change_move_status(True)
                            move_list.append((single_animal, neighbor))
            current_herd.herd_list = [single_animal for single_animal
                                      in current_herd.herd_list
                                      if single_animal not in dict(move_list)]
        return move_list

    def get_all_herds(self):
        """
        Get all the herds in the biome.

        Returns
        -------
        list
            list of all herd
        """

        return self._all_herds

    def get_landscape(self):
        """
        Gets the landscape.

        Returns
        -------
        Landscape
            landscape type of biome
        """

        return self._landscape

    def get_total_herbivore_num(self):
        """
        Gets the total herbivore number from herd list

        Returns
        -------
        int
            total number of herbivores

        """
        herb_n = 0
        if self._all_herds:
            herb_n = sum([len(current_herd.herd_list) for current_herd in self._all_herds
                          if (current_herd.is_predator is False)])
        return herb_n

    def get_total_carnivore_num(self):
        """
        Gets the total number of carnivores form herd list

        Returns
        -------
        int
            total number of carnivores
        """
        carn_n = 0
        if self._all_herds:
            carn_n = sum([len(current_herd.herd_list) for current_herd in self._all_herds
                          if (current_herd.is_predator is True)])
        return carn_n

    def get_total_animal_num(self):
        """
        Gets the total numbers of animals in the herd

        Returns
        -------
        int
            total number of animals

        """
        total_num = 0
        if self._all_herds:
            total_num = sum([len(current_herd.herd_list) for current_herd in self._all_herds])
        return total_num
