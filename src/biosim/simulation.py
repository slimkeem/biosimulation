from biosim.island import Island

import random
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import numpy as np
import os
import subprocess
"""
Template for BioSim class.
"""

# The material in this file is licensed under the BSD 3-clause license
# https://opensource.org/licenses/BSD-3-Clause
# (C) Copyright 2021 Hans Ekkehard Plesser / NMBU

__author__ = 'AbdulHakeem Ayodele Adamu, Atif Fazal'
__email__ = 'abdulhakeem.ayodele.adamu@nmbu.no, atif.fazal@nmbu.no'

_FFMPEG_BINARY = 'ffmpeg'
_MAGICK_BINARY = 'magick'
_DEFAULT_GRAPHICS_NAME = 'dv'
_DEFAULT_GRAPHICS_DIR = os.path.join('..', 'data')
_DEFAULT_IMG_FORMAT = 'png'
_DEFAULT_MOVIE_FORMAT = 'mp4'   # alternatives: mp4, gif
_RGB_VALUES = {"W": (0.0, 0.0, 1.0),
               "H": (0.0, 0.6, 0.0),
               "L": (0.5, 1.0, 0.5),
               "D": (1.0, 1.0, 0.5)}


class BioSim:
    def __init__(self, island_map, ini_pop, seed,
                 vis_years=1, ymax_animals=None, cmax_animals=None, hist_specs=None,
                 img_dir=None, img_base=None, img_fmt='png', img_years=None,
                 log_file=None):

        """
        :param island_map: Multi-line string specifying island geography
        :param ini_pop: List of dictionaries specifying initial population
        :param seed: Integer used as random number seed
        :param ymax_animals: Number specifying y-axis limit for graph showing animal numbers
        :param cmax_animals: Dict specifying color-code limits for animal densities
        :param hist_specs: Specifications for histograms, see below
        :param vis_years: years between visualization updates (if 0, disable graphics)
        :param img_dir: String with path to directory for figures
        :param img_base: String with beginning of file name for figures
        :param img_fmt: String with file type for figures, e.g. 'png'
        :param img_years: years between visualizations saved to files (default: vis_years)
        :param log_file: If given, write animal counts to this file

        If ymax_animals is None, the y-axis limit should be adjusted automatically.
        If cmax_animals is None, sensible, fixed default values should be used.
        cmax_animals is a dict mapping species names to numbers, e.g.,
           {'Herbivore': 50, 'Carnivore': 20}

        hist_specs is a dictionary with one entry per property for which a histogram shall be shown.
        For each property, a dictionary providing the maximum value and the bin width must be
        given, e.g.,
            {'weight': {'max': 80, 'delta': 2}, 'fitness': {'max': 1.0, 'delta': 0.05}}
        Permitted properties are 'weight', 'age', 'fitness'.

        If img_dir is None, no figures are written to file. Filenames are formed as

            f'{os.path.join(img_dir, img_base}_{img_number:05d}.{img_fmt}'

        where img_number are consecutive image numbers starting from 0.

        img_dir and img_base must either be both None or both strings.
        """

        # set seeds
        np.random.seed(seed)
        random.seed(seed)

        # Set properties from constructor
        self._vis_years = vis_years
        self._ymax_animals = ymax_animals

        # initialize objects
        self._island = Island(island_map, ini_pop)

        # Set defaults:
        if img_years is None:
            self._img_years = vis_years
        else:
            self._img_years = img_years
        if cmax_animals is not None:
            self._cmax_animals = cmax_animals
        else:
            self._cmax_animals = {'Herbivore': 50, 'Carnivore': 20}
        if img_base is None:
            img_base = _DEFAULT_GRAPHICS_NAME
        if img_dir is not None:
            self._img_base = os.path.join(img_dir, img_base)
        else:
            self._img_base = None
        if img_fmt is not None:
            self._img_fmt = img_fmt
        else:
            self._img_fmt = _DEFAULT_IMG_FORMAT

        # Create the following properties
        self._island_grid_rgb = [[_RGB_VALUES[column] for column in row.strip()]
                                 for row in island_map.splitlines()]
        self._year = 0
        self._final_year = None
        self._herbs_n_2d = None
        self._carns_n_2d = None
        self._num_animals_per_species = None
        self._num_animals = None
        self._img_count = 0
        self._img_year = 1
        # for plotting
        self._fig = None
        self._gs = None
        self._island_2d_matrix_ax = None
        self._herbs_heatmap_ax = None
        self._herbs_axis = None
        self._carns_heatmap_ax = None
        self._carns_axis = None
        self._all_animals_count_ax = None
        self._herbs_count_line = None
        self._carns_count_line = None
        self._year_ax = "Year: {:5d}"
        self._year_counter_active = False

    # Private
    # VOID
    def _setup_graphics(self):
        """
        Prepare graphics
        Call this before calling :meth:`update()` for the first time after
        the final time step has changed
        """

        # create new figure window
        if self._fig is None:
            self._fig = plt.figure(constrained_layout=True)
            self._gs = gridspec.GridSpec(ncols=3, nrows=3, figure=self._fig)

        # Plotting for the year counter
        if not self._year_counter_active:
            self._txt = self._fig.text(
                0.5, 0.64,
                self._year_ax.format(0),
                ha="center", va="center",
                bbox=dict(boxstyle="round", ec=(0, 0, 0), fc="none",))
            self._year_counter_active = True

        # Plotting the island grid
        if self._island_2d_matrix_ax is None:
            self._island_2d_matrix_ax = self._fig.add_subplot(self._gs[0, 1])
            self._island_2d_matrix_ax.set_title('The Island')
            self._island_2d_matrix_ax.imshow(self._island_grid_rgb,
                                             interpolation='nearest',
                                             vmin=-0.25, vmax=0.25)
            labels = ["Water", "Highland", "Lowland", "Desert"]
            patches = [mpatches.Patch(color=_RGB_VALUES[i], label=labels[n])
                       for n, i in enumerate(_RGB_VALUES.keys())]
            self._island_2d_matrix_ax.legend(handles=patches, prop={"size": 5}, loc=4)

        # Plotting the heatmap for herbivores
        if self._herbs_heatmap_ax is None:
            self._herbs_heatmap_ax = self._fig.add_subplot(self._gs[0, 0])
            self._herbs_heatmap_ax.set_title('Herbivore Distribution')
            self._herbs_axis = None

        # Plotting the heatmap for carnivores
        if self._carns_heatmap_ax is None:
            self._carns_heatmap_ax = self._fig.add_subplot(self._gs[0, -1])
            self._carns_heatmap_ax.set_title('Carnivore Distribution')
            self._carns_axis = None

        # Plotting the count of animals
        if self._all_animals_count_ax is None:
            self._all_animals_count_ax = self._fig.add_subplot(self._gs[-1, :])
            self._all_animals_count_ax.set_title('All Animals Count')
            if self._ymax_animals is None:
                self._ymax_animals = self.num_animals
            self._all_animals_count_ax.set_ylim(0, self._ymax_animals+100)
        # for updating on subsequent calls to simulate()
        self._all_animals_count_ax.set_xlim(0, self._final_year + 1)

        # The herbivore line which is used when plotting the count of herbivores
        if self._herbs_count_line is None:
            herbs_count_plot = self._all_animals_count_ax.plot(
                np.arange(0, self._final_year + 1),
                np.full(self._final_year + 1, np.nan))
            self._herbs_count_line = herbs_count_plot[0]
        else:
            x_data, y_data = self._herbs_count_line.get_data()
            x_new = np.arange(x_data[-1] + 1, self._final_year + 1)
            if len(x_new) > 0:
                y_new = np.full(x_new.shape, np.nan)
                self._herbs_count_line.set_data(np.hstack((x_data, x_new)),
                                                np.hstack((y_data, y_new)))

        # The carnivore line which is used when plotting the count of herbivores
        if self._carns_count_line is None:
            carns_count_plot = self._all_animals_count_ax.plot(
                np.arange(0, self._final_year + 1),
                np.full(self._final_year + 1, np.nan))
            self._carns_count_line = carns_count_plot[0]
        else:
            x_data, y_data = self._carns_count_line.get_data()
            x_new = np.arange(x_data[-1] + 1, self._final_year + 1)
            if len(x_new) > 0:
                y_new = np.full(x_new.shape, np.nan)
                self._carns_count_line.set_data(np.hstack((x_data, x_new)),
                                                np.hstack((y_data, y_new)))

        # Herbivore

        # if self._fitness_histogram is None:
        #     f_ax5 = fig.add_subplot(gs[1, 0])
        #     f_ax5.set_title('Fitness Histogram')
        # if self._age_histogram is None:
        #     f_ax6 = fig.add_subplot(gs[1, 1])
        #     f_ax6.set_title('Age Histogram')
        # if self._weight_histogram is None:
        #     f_ax7 = fig.add_subplot(gs[1, 2])
        #     f_ax7.set_title('Weight Histogram')

    def _update_graphics(self):
        """ Updates graphics with current data and save to file if necessary. """
        self._update_herb_heatmap()
        self._update_carn_heatmap()
        self._update_count_graph()
        self._txt.set_text(self._year_ax.format(self._year))

        # self._update_hist(sys_map)
        self._fig.canvas.flush_events()  # ensure every thing is drawn
        plt.pause(1e-6)  # pause required to pass control to GUI

    def _update_herb_heatmap(self):
        """Update the 2D-view of the system."""

        if self._herbs_axis is not None:
            self._herbs_axis.set_data(self.herbs_n_2d)
        else:
            self._herbs_axis = self._herbs_heatmap_ax.imshow(
                self.herbs_n_2d, interpolation='nearest',
                vmin=0, vmax=self._cmax_animals['Herbivore'])
            plt.colorbar(self._herbs_axis, ax=self._herbs_heatmap_ax,
                         orientation='horizontal')

    def _update_carn_heatmap(self):
        """Update the 2D-view of the system."""

        if self._carns_axis is not None:
            self._carns_axis.set_data(self.carns_n_2d)
        else:
            self._carns_axis = \
                self._carns_heatmap_ax.imshow(self.carns_n_2d,
                                              interpolation='nearest',
                                              vmin=0, vmax=self._cmax_animals['Carnivore'])
            plt.colorbar(self._carns_axis, ax=self._carns_heatmap_ax,
                         orientation='horizontal')

    def _update_count_graph(self):
        ydata_herb = self._herbs_count_line.get_ydata()
        ydata_herb[self._year] = self.num_animals_per_species["Herbivore"]
        self._herbs_count_line.set_ydata(ydata_herb)

        ydata_carn = self._carns_count_line.get_ydata()
        ydata_carn[self._year] = self.num_animals_per_species["Carnivore"]
        self._carns_count_line.set_ydata(ydata_carn)
        if self._ymax_animals < self.num_animals:
            self._ymax_animals = self.num_animals
        self._all_animals_count_ax.set_ylim(0, self._ymax_animals+100)
        self._all_animals_count_ax.legend(["Herbivore", "Carnivore"], prop={"size": 6})

    def _update_hist(self, sys_map):
        counts = np.histogram(sys_map.flat, self._limits)[0]
        self._hist_line.set_ydata(counts)
        self._ymax = max(self._ymax, 1.05*max(counts))
        self._hist_ax.set_ylim(0, self._ymax)

    def _save_graphics(self):
        plt.savefig("{img_base}_{img_count:05d}.{img_fmt}".format(
            img_base=self._img_base, img_count=self._img_count, img_fmt=self._img_fmt))

        self._img_count += 1

    def set_animal_parameters(self, species, params):
        """
        Set parameters for animal species.

        :param species: String, name of animal species
        :param params: Dict with valid parameter specification for species
        """

        self._island.set_animal_params(species, params)

    def set_landscape_parameters(self, landscape, params):
        """
        Set parameters for landscape type.

        :param landscape: String, code letter for landscape
        :param params: Dict with valid parameter specification for landscape
        """

        self._island.set_landscape_params(landscape, params)

    def simulate(self, num_years):
        """
        Run simulation while visualizing the result.

        Parameters
        ----------
        num_years : number of years to simulate
        """

        self._final_year = self._year + num_years

        if self._vis_years == 0:
            while self._year < self._final_year:
                self._island.run_cycle()
                self._year += 1

        else:
            if self._img_years % self._vis_years != 0:
                raise ValueError('img_years must be multiple of vis_years')

            self._setup_graphics()

            while self._year < self._final_year:
                if self._year % self._vis_years == 0:
                    self._update_graphics()
                if self._img_base is not None or self._year % self._img_year == 0:
                    self._save_graphics()
                self._island.run_cycle()
                self._year += 1

    def add_population(self, population):
        """
        Add a population to the island

        :param population: List of dictionaries specifying population
        """

        self._island.add_herd(population)

    @property
    def year(self):
        """Last year simulated."""

        return self._year

    @property
    def num_animals(self):
        """Total number of animals on island."""

        self._num_animals = self._island.get_total_animals()
        return self._num_animals

    @property
    def herbs_n_2d(self):
        self._herbs_n_2d = self._island.herbs_matrix
        return self._herbs_n_2d

    @property
    def carns_n_2d(self):
        self._carns_n_2d = self._island.carns_matrix
        return self._carns_n_2d

    @property
    def num_animals_per_species(self):
        """Number of animals per species in island, as dictionary."""

        self._num_animals_per_species = self._island.get_population_num_dict()
        return self._num_animals_per_species

    def make_movie(self, movie_fmt=_DEFAULT_MOVIE_FORMAT):
        if self._img_base is None:
            raise RuntimeError("No filename defined.")

        if movie_fmt == "mp4":
            try:
                # Parameters chosen according to
                # http://trac.ffmpeg.org/wiki/Encode/H.264, section
                # "Compatibility"
                subprocess.check_call([_FFMPEG_BINARY, "-i",
                                       "{}_%05d.png".format(self._img_base),
                                       "-y", "-profile:v", "baseline", "-level",
                                       "3.0", "-pix_fmt", "yuv420p",
                                       "{}.{}".format(self._img_base, movie_fmt)])
            except subprocess.CalledProcessError as err:
                raise RuntimeError("ERROR: ffmpeg failed with: {}".format(err))
        elif movie_fmt == "gif":
            try:
                subprocess.check_call([_MAGICK_BINARY, "-delay", "1", "-loop",
                                       "0", "{}_*.png".format(self._img_base),
                                       "{}.{}".format(self._img_base, movie_fmt)])
            except subprocess.CalledProcessError as err:
                raise RuntimeError("ERROR: convert failed with: {}".format(err))
        else:
            raise ValueError("Unknown movie format: " + movie_fmt)
