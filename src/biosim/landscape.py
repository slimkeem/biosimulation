# -*- codingK utf-8 -*-

"""
Python package for biosim
"""

__author__ = 'AbdulHakeem Ayodele Adamu, Atif Fazal'
__email__ = 'abdulhakeem.ayodele.adamu@nmbu.no, atif.fazal@nmbu.no'

"""
Implements a model for Landscape and its subclasses
"""


class Landscape:
    """Landscape with different amounts of fodder"""

    params = {}

    @classmethod
    def set_params(cls, param_dict):
        """
        Sets class parameters.

        Parameters
        ----------
        param_dict : dict
            keys: 'f_max', 'habitability'
        """

        cls.params.update(param_dict)

    @classmethod
    def get_params(cls):
        """
        Gets the class parameters.

        Returns
        -------
        dict
            Dictionary with the class parameters
        """

        return cls.params

    def __init__(self):
        """Initializes the Landscape (base) class"""

        self._habitability = None
        self._set_fodder()
        self._set_fertility_check()

    def _set_fodder(self):
        """Sets the fodder of a Landscape"""

        self._current_fodder = self.params['f_max']

    def _set_fertility_check(self):
        """Checks if the Landscape has fodder or not"""

        if self.params['f_max'] > 0:
            self._fertility_check = True
        else:
            self._fertility_check = False

    # Public
    # VOID
    def update_yearly_fodder(self):
        """Updates the fodder every cycle"""

        self._set_fodder()

    def reduce_eaten_fodder(self, amount_eaten):
        """
        Reduces the amount of fodder available based on what is eaten.

        Parameters
        ----------
        amount_eaten : int
            Amount of fodder that has been eaten
        """

        self._current_fodder -= amount_eaten

    # NON-VOID
    def get_current_fodder(self):
        """
        Gets the current fodder for the Landscape

        Returns
        -------
        int
            current fodder
        """

        return self._current_fodder

    def get_fertility_check(self):
        """
        Get the fertility check, if there is fodder or not

        Returns
        -------
        bool
            True if there is fodder
            False otherwise
        """

        return self._fertility_check

    def get_habitability(self):
        """
        Gets the habitability of a Landscape type

        Returns
        -------
        bool
            True if habitable
            False otherwise
        """

        return self._habitability


class Water(Landscape):
    """Water: Subclass of Landscape"""

    params = {'f_max': 0}

    def __init__(self):
        """Initializes the Water (sub) class by running init of parent class"""

        super(Water, self).__init__()
        self._habitability = False


class Lowland(Landscape):
    """Lowland: Subclass of Landscape """

    params = {'f_max': 800}

    def __init__(self):
        """Initializes the Lowland (sub) class by running init of parent class"""

        super(Lowland, self).__init__()
        self._habitability = True


class Highland(Landscape):
    """Highland: Subclass of Landscape"""

    params = {'f_max': 300}

    def __init__(self):
        """Initializes the Highland (sub) class by running init of parent class"""

        super(Highland, self).__init__()
        self._habitability = True


class Desert(Landscape):
    """Desert: Subclass of Landscape"""

    params = {'f_max': 0}

    def __init__(self):
        """Initializes the Desert (sub) class by running init of parent class"""

        super(Desert, self).__init__()
        self._habitability = True
