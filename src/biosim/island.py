# -*- coding utf-8 -*-

"""
Python package for biosim
"""

__author__ = 'AbdulHakeem Ayodele Adamu, Atif Fazal'
__email__ = 'abdulhakeem.ayodele.adamu@nmbu.no, atif.fazal@nmbu.no'

from dataclasses import dataclass
import numpy as np

from biosim.biome import Biome
from biosim.landscape import Water
import biosim.helper as helper

"""
Implements a model for an island
"""


@dataclass
class Cell:
    """Contains a mapping of a biome object and its corresponding key on the island"""

    cell_key: tuple
    cell_biome: Biome


class Island:
    """Class which creates an island"""

    def __init__(self, grid, herds_string):
        """
        Initializes the island class

        Parameters
        ----------
        grid: str
            grid of island, string of landscape types

        herds_string: list
            list containing a dictionary with the location and population of animals
        """

        # properties
        self._herbs_matrix = None
        self._carns_matrix = None

        self._set_island_grid(grid, herds_string)
        self._set_neighbor_biomes()

    # Private Functions
    # VOID
    def _set_island_grid(self, grid, herds_string):
        """
        Sets up the island grid: Extracting the landscape and herds
        from its parameters and places them in biomes,
        and maps the biomes into cells.

        Parameters
        ----------
        grid: str
            grid of island, string of landscape types

        herds_string: list
            list containing a dictionary with the location and population of animals
        """

        self._island_grid = []
        landscape_dict = helper.extract_landscape_dict(grid)
        herd_dict = helper.extract_herd_dict(herds_string)

        boundary_cells = helper.get_boundary_cells(landscape_dict.keys())
        for boundary_cell in boundary_cells:
            if type(landscape_dict.get(boundary_cell)) != Water:
                raise ValueError("invalid Boundary")

        for landscape_key in landscape_dict.keys():
            if landscape_key not in herd_dict.keys():
                new_biome = Biome(landscape_dict.get(landscape_key))
                new_grid_cell = Cell(landscape_key, new_biome)
                self._island_grid.append(new_grid_cell)
            else:
                new_biome = Biome(landscape_dict.get(landscape_key),
                                  herd_dict.get(landscape_key))
                new_grid_cell = Cell(landscape_key, new_biome)
                self._island_grid.append(new_grid_cell)

    def _set_neighbor_biomes(self):
        """Sets the neighbouring biome coordinates from the surrounding cells"""

        for grid_cell in self._island_grid:
            cell_biome = grid_cell.cell_biome
            cell_key = grid_cell.cell_key
            neighbors_coords = helper.get_neighbor_coords(cell_key, self.get_keys())
            neighbor_biomes = []

            # Get neighbor-biomes from coordinates
            for coords in neighbors_coords:
                neighbor_biomes.append(self._get_biome(coords))

            # Set neighbor-biomes of every biome in grid
            cell_biome.set_neighbors(neighbor_biomes)

    # NON-VOID
    def _get_biome(self, coords):
        """
        Gets the biome associated to the coordinates (keys) from the cell

        Parameters
        ----------
        coords : tuple
            the key associated to a biome in a cell

        Returns
        ------
        Biome
        """

        for grid_cell in self._island_grid:
            if grid_cell.cell_key == coords:
                return grid_cell.cell_biome

    @property
    def herbs_matrix(self):
        herbs_matrix = np.zeros(self.get_2d_grid_size())
        for i, grid_cell in enumerate(self.get_island_grid()):
            cell_biome = grid_cell.cell_biome
            cell_key = grid_cell.cell_key
            x = cell_key[0]-1
            y = cell_key[1]-1
            herbs_matrix[x, y] = cell_biome.get_total_herbivore_num()
        self._herbs_matrix = herbs_matrix
        return self._herbs_matrix

    @property
    def carns_matrix(self):
        carns_matrix = np.empty(self.get_2d_grid_size())
        for i, grid_cell in enumerate(self.get_island_grid()):
            cell_biome = grid_cell.cell_biome
            cell_key = grid_cell.cell_key
            x = cell_key[0] - 1
            y = cell_key[1] - 1
            carns_matrix[x, y] = cell_biome.get_total_carnivore_num()
        self._carns_matrix = carns_matrix
        return self._carns_matrix

    def get_keys(self):
        all_keys = []
        for grid_cell in self._island_grid:
            all_keys.append(grid_cell.cell_key)
        return all_keys

    # Public Functions
    # VOID
    def add_herd(self, population):
        """
        Adds additional animals to the herd

        Parameters
        ----------
        population: dict
            Additional animals to add
        """
        herd_dict = helper.extract_herd_dict(population)

        for cell_key in herd_dict.keys():
            herd_list = herd_dict.get(cell_key)
            recipient_biome = self._get_biome(cell_key)
            for herd in herd_list:
                for animal in herd.herd_list:
                    recipient_biome.immigrate(animal)

    def run_cycle(self):
        """
        Updates all interactions in the cells and between cells for one cycle for every year
        """

        new_year = True
        for grid_cell in self._island_grid:
            cell_biome = grid_cell.cell_biome
            # # CYCLE
            if cell_biome.get_landscape().get_fertility_check():
                cell_biome.replenish_fodder()
            if cell_biome.get_all_herds():
                cell_biome.feed()
                cell_biome.procreate()
                if new_year:
                    cell_biome.reset_yearly_migration_status()
                move_list = cell_biome.emigrate()
                if move_list:
                    for moving_animal, immigrating_biome in move_list:
                        immigrating_biome.immigrate(moving_animal)
                cell_biome.age()
                cell_biome.lose_yearly_weight()
                cell_biome.die()
                new_year = False

    @staticmethod
    def set_animal_params(species, params):
        """
        Sets the animal parameters

        Parameters
        ----------
        species
            animal specie

        params: dict
            animal parameters
        """
        helper.set_animal_params(species, params)

    @staticmethod
    def set_landscape_params(landscape, params):
        """
        Sets th landscape parameters

        Parameters
        ----------
        landscape: str
            first letter of landscape

        params: dict
            landscape parameters
        """
        helper.set_landscape_params(landscape, params)

    # NON-VOID
    def get_island_grid(self):
        """
        Gets the island grid

        Returns
        -------
        list
            island grid
        """

        return self._island_grid

    def get_population_num_dict(self):
        """
        Gets the total number of herbivore and carnivores
        Returns
        -------
        dict
            dictionary with total herbivore and carnivore numbers
        """

        herb_num = 0
        carn_num = 0
        for grid_cell in self._island_grid:
            cell_biome = grid_cell.cell_biome
            herb_num += cell_biome.get_total_herbivore_num()
            carn_num += cell_biome.get_total_carnivore_num()
        pop_dict = {'Herbivore': herb_num,
                    'Carnivore': carn_num}
        return pop_dict

    def get_total_animals(self):
        """
        Gets total animals on island

        Returns
        -------
        int
            total number of animals
        """

        total_num = 0
        for grid_cell in self._island_grid:
            cell_biome = grid_cell.cell_biome
            total_num += cell_biome.get_total_animal_num()
        return total_num

    def get_2d_grid_size(self):
        """ Returns the size of the island in a 2d matrix form"""
        max_col_key = max(self.get_keys(), key=lambda key: key[1])[1]
        max_row_key = max(self.get_keys(), key=lambda key: key[0])[0]

        return max_row_key, max_col_key
