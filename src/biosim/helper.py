# -*- coding utf-8 -*-

"""
Python package for biosim
"""

__author__ = 'AbdulHakeem Ayodele Adamu, Atif Fazal'
__email__ = 'abdulhakeem.ayodele.adamu@nmbu.no, atif.fazal@nmbu.no'

import biosim.biome as biome
import biosim.animal as animal
import biosim.landscape as landscape

landscape_dict = {
    'W': landscape.Water,
    'L': landscape.Lowland,
    'D': landscape.Desert,
    'H': landscape.Highland
}

animal_dict = {
    'Herbivore': animal.Herbivore,
    'Carnivore': animal.Carnivore
}


def set_animal_params(species, params):
    animal_dict[species].set_params(params)


def set_landscape_params(landscape_code, params):
    """
    picks the right landscape and sets the parameters for
    the landscape class from a landscape code, L,H,W or D

    Parameters
    ----------
    landscape_code : str
        first letter of a landscape

    params : dict
        the parameters to update the class with.
    """
    landscape_dict[landscape_code].set_params(params)


def _create_new_herd_from_dict(animals_dict):
    """
    Creates all the herds in the biome based on their type

    Parameters
    ----------
    animals_dict : Dict[str, Tuple[int, int]]
        a dictionary of the type of animals and their position

    Raises
    ------
    TypeError

    Returns
    -------
    biosim.biome.Herd
    """

    new_population = animals_dict['pop']
    herd_list = []
    is_predator = None
    for new_animal in new_population:
        if new_animal['species'] == 'Herbivore':
            is_predator = False
            herd_list.append(animal.Herbivore(age=new_animal['age'],
                                              weight=new_animal['weight']))
        elif new_animal['species'] == 'Carnivore':
            is_predator = True
            herd_list.append(animal.Carnivore(age=new_animal['age'],
                                              weight=new_animal['weight']))
        else:
            TypeError("Has to be a herbivore or carnivore")
    return biome.Herd(is_predator, herd_list)


def extract_landscape_dict(geogr_dict):
    """
    Creates a dictionary of landscapes with the respective coordinates as the key

    Parameters
    ----------
    geogr_dict : str
        the string from which to map all the different landscapes

    Returns
    -------
    Union[Dict[Tuple[int, int], biosim.landscape.Landscape]]
    """

    landscape_key_pair = {}
    prev_row_length = None
    for col, row in enumerate(geogr_dict.split('\n')):
        if prev_row_length is not None and len(row.strip()) != prev_row_length:
            raise ValueError("inconsistent lengths for grid")
        else:
            for val in row.strip():
                if val not in landscape_dict.keys():
                    raise ValueError(val)
            new_landscape_key_pair = {(col + 1, idx + 1): landscape_dict[val]()
                                      for idx, val in enumerate([col, row.strip()][1])}
            landscape_key_pair.update(new_landscape_key_pair)
            prev_row_length = len(row.strip())
    return landscape_key_pair


def get_boundary_cells(keys):
    min_col = 1
    max_col = max([item for key in keys for item in key])
    boundary_keys = [key for key in keys
                     if (key[0] == min_col or key[0] == max_col)
                     or (key[1] == min_col or key[1] == max_col)]
    return boundary_keys


def extract_herd_dict(herd_string):
    """
    creates a dictionary of herds with the respective coordinates as the key

    Parameters
    ----------
    herd_string : List[Dict[str, Tuple[int, int]]]
        the initial population and location to extract from

    Returns
    -------
        Dict[Tuple[int, int], List[biosim.biome.Herd]]
    """

    herd_key_pair = {}
    for new_dict in herd_string:
        new_herd = _create_new_herd_from_dict(new_dict)
        new_key = new_dict['loc']
        if new_key not in herd_key_pair:
            herd_key_pair.update({new_key: [new_herd]})
        else:
            if new_herd.is_predator not in [old_herd.is_predator
                                            for old_herd
                                            in herd_key_pair.get(new_key)]:
                herd_key_pair.get(new_key).append(new_herd)
            else:
                for old_herd in herd_key_pair.get(new_key):
                    if old_herd.is_predator == new_herd.is_predator:
                        while new_herd.herd_list:
                            old_herd.herd_list.append(new_herd.herd_list.pop())
    return herd_key_pair


def get_neighbor_coords(cell_key, all_keys=None):
    """
    Gets the neighboring cells

    Parameters
    ----------
    cell_key : Tuple[int, int]
        coordinate of the current cell
    all_keys : List
        list of all cell-keys in the islands

    Returns
    -------
    List[Tuple[int, int]]
    """

    x, y = cell_key
    possible_coords = [(x+1, y), (x, y+1), (x-1, y), (x, y-1)]
    neighbor_coords = [current_coord for current_coord in possible_coords
                       if (current_coord in all_keys)]
    return neighbor_coords
