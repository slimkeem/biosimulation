# -*- codingK utf-8 -*-

"""
Python package for biosim
"""

__author__ = 'AbdulHakeem Ayodele Adamu, Atif Fazal'
__email__ = 'abdulhakeem.ayodele.adamu@nmbu.no, atif.fazal@nmbu.no'
__version__ = '1.0'
